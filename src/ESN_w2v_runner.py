
import logging
import CONFIG
from ESN import AppInventorEncoder
import os
from parser import *
from zipfile import BadZipfile
import multiprocessing
import itertools
import subprocess
from subprocess import Popen, PIPE, STDOUT, call
import sys
from threading import Timer

import pickle
import pandas as pd
import numpy as np
import random
from visualiziation import blockrep_to_dot
from visualiziation import xml_to_dot, visualize_attention_test
import time

import matplotlib.pyplot as plt
from sklearn.decomposition import PCA


from argparse import ArgumentParser
import shutil


from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering


def blocks_rep_to_terminal_nodes(parents_dict):

    # every element in the dict's keys that is not also in the dict's values is a terminal node
    keys_set = set(parents_dict.keys())
    values_set = []
    for value in parents_dict.values():
        if isinstance(value,tuple):
            values_set.append(value[0])
        else:
            values_set.append(value)
    values_set = set(values_set)

    nodes_set = keys_set.difference(values_set)
    return nodes_set


def get_root_path(node, index, top_block_representation):
    path = [top_block_representation[2][index]]

    current_index = index
    child_matrix = top_block_representation[1]

    # if index is 0 then we reached root
    while current_index != 0:
        for node_index, node_childs in enumerate(child_matrix):
            if current_index in node_childs:
                path.append(top_block_representation[2][node_index])
                current_index = node_index
    return path


def get_terminal_paths(terminal_nodes, parents_dict, path_max_len, max_paths):
    paths = []
    terminal_nodes_list = list(terminal_nodes)
    random.shuffle(terminal_nodes_list)

    for i in range(len(terminal_nodes_list)):
        first_node = terminal_nodes_list[i]
        first_node_parents = parents_dict[first_node][1]
        first_node_parents.reverse()
        for j in range(len(terminal_nodes_list)):
            if i != j:
                second_node = terminal_nodes_list[j]
                second_node_parents = parents_dict[second_node][1]

                terminal_path = get_terminal_path(first_node, second_node, first_node_parents, second_node_parents, path_max_len)
                if terminal_path is None:
                    continue
                paths.append(terminal_path)

                if len(paths) == max_paths:
                    return paths

    return paths


def get_path_string(terminal_path):
    wrong_chars = [" ", ","]

    path_string = ""
    start_token = terminal_path[0].tag
    for char in wrong_chars: start_token = start_token.replace(char, "")

    path_string += start_token + ','

    # dont want to iterate over the terminal nodes of the path
    for item in terminal_path[1:-1]:
        path_string += item if isinstance(item,str) else ('(' + item.tag + ')')

    path_string += ','

    end_token = terminal_path[-1].tag
    for char in wrong_chars: end_token = end_token.replace(char, "")
    path_string += end_token
    return path_string.encode('ascii', 'ignore').decode('ascii')

def get_code2vec_inputfile(labels, parents_dicts, all_terminal_nodes, path_max_len = 8, max_paths = 200):
    towrite = ""
    num_examples = len(labels)
    for i in range(num_examples):
        if i % 100 == 0:
            print("In example number: ", str(i), "out of: ", str(num_examples))

        # terminal_nodes = blocks_rep_to_terminal_nodes(parents_dicts[i])
        terminal_nodes = all_terminal_nodes[i]

        # FOR DEBUGGING, check if all terminal nodes got a path in the paths dict
        # for node_index, terminal_node in enumerate(terminal_nodes):
        #     try:
        #         father_path = parent_dict[terminal_node]
        #         path = father_path[1]
        #         if not isinstance(path, list):
        #             x = 1
        #     except Exception as e:
        #         s = 1

        terminal_paths = get_terminal_paths(terminal_nodes, parents_dicts[i], path_max_len, max_paths)

        if not terminal_paths:
            continue

        towrite += (str(labels[i]) + " ")

        for terminal_path in terminal_paths:
            towrite += get_path_string(terminal_path)
            towrite += " "

        towrite.rstrip()
        towrite += "\n"
    return towrite



def main(args):
    logging.basicConfig(level=logging.ERROR)

    start_time = time.time()


    # if os.path.exists("../results/kmeans_200.pkl"):
    #     kmeans = pickle.load(open("../results/kmeans_200.pkl", "rb"))
    # pickle.dump(kmeans.cluster_centers_, open("../results/cluster_centers.pkl", "wb"))


    # embeddings = pd.read_csv("../results/10k_users_embeddings_train.csv")
    # kmeans = pickle.load(open("../results/kmeans_200.pkl", "rb"))

    # data = PCA(n_components=2).fit_transform(embeddings)
    #
    # labels =kmeans.labels_
    # for label, color in zip(range(3), ['r', 'g', 'b']):
    #     plt.scatter(data[labels == label, 0], data[labels == label, 1], c=color)
    # plt.title("Clusters on reduced data")
    # plt.show()


    # unique, counts = np.unique(kmeans.labels_, return_counts=True)
    # print(np.asarray((unique, counts)).T)


    encodings, top_block_representations, parents_dicts = generate_embeddings(args.dir)
    labels = get_labels_by_kmeans(encodings)

    # labeled_1_projects = []
    # for label_index in range(len(labels)):
    #     if label_index == 1:
    #         labeled_1_projects.append(top_block_representations[label_index])
    #
    #
    # random_top_blocks = random.choices(labeled_1_projects, k=10)
    # pickle.dump(random_top_blocks, open("../results/random_labeled1_topblocks.pkl", "wb"))
    # print(random_top_blocks)

    # labeled_2_projects = []
    # for label_index in range(len(labels)):
    #     if label_index == 2:
    #         labeled_2_projects.append(top_block_representations[label_index])
    #
    # random_top_blocks = random.choices(labeled_2_projects, k=10)
    # pickle.dump(random_top_blocks, open("../results/random_labeled2_topblocks.pkl", "wb"))
    # print(random_top_blocks)
    #
    # labeled_50_projects = []
    # for label_index in range(len(labels)):
    #     if label_index == 50:
    #         labeled_50_projects.append(top_block_representations[label_index])
    #
    # random_top_blocks = random.choices(labeled_50_projects, k=10)
    # pickle.dump(random_top_blocks, open("../results/random_labeled50_topblocks.pkl", "wb"))
    # print(random_top_blocks)
    #
    # labeled_91_projects = []
    # for label_index in range(len(labels)):
    #     if label_index == 91:
    #         labeled_91_projects.append(top_block_representations[label_index])
    #
    # random_top_blocks = random.choices(labeled_91_projects, k=10)
    # pickle.dump(random_top_blocks, open("../results/random_labeled91_topblocks.pkl", "wb"))
    # print(random_top_blocks)

    del top_block_representations
    del encodings

    # num_chunks = 10
    #
    # parents_dicts_chunks = np.array_split(np.array(parents_dicts), num_chunks)
    # del parents_dicts
    #
    # labels_chunks = np.array_split(labels, num_chunks)
    # del labels


    # for index, parents_dicts, labels in zip(range(num_chunks),parents_dicts_chunks, labels_chunks):
    to_write = get_code2vec_inputfile(labels, parents_dicts)

    with open(args.file, "a+") as text_file:
        to_write.rstrip('\n')
        text_file.write(to_write)

        # del parents_dicts_chunks[index]
        # del labels_chunks[index]



    end_time = time.time()
    print("Time in miliseconds: ", str(end_time - start_time))




def get_labels_by_kmeans(encodings):
    if os.path.exists("../results/kmeans_200.pkl"):
        kmeans = pickle.load(open("../results/kmeans_200.pkl", "rb"))
        labels = kmeans.fit_predict(encodings)
        return labels

    # Sum_of_squared_distances = []
    # K = range(50, 400, 25)
    # for k in K:
    #     kmeans = KMeans(n_clusters=k,)
    #     kmeans = kmeans.fit(encodings)
    #     Sum_of_squared_distances.append(kmeans.inertia_)
    #
    # plt.plot(K, Sum_of_squared_distances, 'bx-')
    # plt.xlabel('k')
    # plt.ylabel('Sum_of_squared_distances')
    # plt.title('Elbow Method For Optimal k')
    # plt.show()



    kmeans = KMeans(n_clusters=200)
    kmeans.fit(encodings)
    pickle.dump(kmeans, open("../results/kmeans_200.pkl", "wb"))

    return kmeans.labels_


def generate_embeddings(projects_dir):
    print("Now starting to generate embeddings with tree ESN.")

    dataset_type = projects_dir.split("_")[-1]

    if os.path.exists("../results/blocks_representations_{}.pkl".format(dataset_type)) and \
            os.path.exists("../results/10k_users_embeddings_{}.csv".format(dataset_type)) and\
            os.path.exists("../results/all_parents_dict_{}.pkl".format(dataset_type)):

        embeddings_df = pd.read_csv("../results/10k_users_embeddings_{}.csv".format(dataset_type))
        top_blocks_representations = pickle.load(open("../results/blocks_representations_{}.pkl".format(dataset_type), "rb"))
        all_parents_dict = pickle.load(open("../results/all_parents_dict_{}.pkl".format(dataset_type), "rb"))

        return embeddings_df, top_blocks_representations, all_parents_dict


    alphabet = set([])
    # this list will contain tuples such that that for each tuple the first element is the nodes list for a top block
    # structure, and the second element is an adjacency matrix for this top block structure.
    top_blocks_representations = []
    projects_path = projects_dir

    all_parents_dict = []
    for users_chunk_folder in os.listdir(projects_path):
        print("Now in chunk ",users_chunk_folder)

        if users_chunk_folder == ".DS_Store":
            continue

        users_chunk_path = os.path.join(projects_path, users_chunk_folder)
        for index, user_folder in enumerate(os.listdir(users_chunk_path)):

            if user_folder == ".DS_Store":
                continue

            if index % 50 == 0:
                print("In user number: ", str(index))

            user_path = os.path.join(users_chunk_path, user_folder)

            for aia_project in os.listdir(user_path):
                absProjDir = os.path.join(user_path, aia_project)

                if aia_project == ".DS_Store":
                    continue

                try:
                    root = proj_to_xml(absProjDir)
                except BadZipfile:
                    print("not a zipfile in path: ")
                    print(absProjDir)
                    continue
                # empty project
                if root is None:
                    continue

                root = arrange_xml(root)
                ## at the moment running it three times, to take care of new childs, TODO: find optimal solution
                root = arrange_xml(root)
                root = arrange_xml(root)

                parents_map = xml_to_parents_map(root)
                dot = xml_to_dot(root)
                parents_dicts_list = parent_map_to_list_maps(parents_map)

                all_parents_dict.extend(parents_dicts_list)

                for parents_dict in parents_dicts_list:
                    nodes_list = parentmap_to_nodeslist(parents_dict)
                    # adj_matrix = create_adj_matrix(nodes_list, parents_dict)

                    tags_set = nodes_to_tags_set(nodes_list)

                    alphabet = alphabet.union(tags_set)

                    top_blocks_representations.append(
                        (nodes_to_tags_list(nodes_list), create_adj_matrix(nodes_list, parents_dict), nodes_list))




    # ESN already exists, so will use it's weights
    if os.path.exists("../results/ESN_object.pkl".format(dataset_type)):
        embedder = pickle.load(open("../results/ESN_object.pkl", "rb"))
        embedder.add_alphabet(alphabet)
        embeddings = []
        unique_encodings = set([])
        for top_block_rep in top_blocks_representations:
            encoding = embedder.encode([top_block_rep])
            embeddings.append(encoding)
            unique_encodings.add(tuple(encoding.tolist()))



    else:
        embedder = AppInventorEncoder(alphabet)
        embeddings = []
        unique_encodings = set([])
        for top_block_rep in top_blocks_representations:
            encoding = embedder.encode([top_block_rep])
            embeddings.append(encoding)
            unique_encodings.add(tuple(encoding.tolist()))

    pickle.dump(embedder, open("../results/ESN_object.pkl", "wb"))
    # pickle.dump(all_parents_dict, open("../results/all_parents_dict_{}.pkl".format(dataset_type), "wb"))

    try:
        df = pd.DataFrame(embeddings)
        df.to_csv("../results/10k_users_embeddings_{}.csv".format(dataset_type))
    except:
        pass

    return embeddings, top_blocks_representations, all_parents_dict

def ParallelExtractDir(args, dir):
    ExtractFeaturesForDir(args, dir, "")

def ExtractFeaturesForDir(args, dir, prefix):
    command = ['java', '-cp', args.jar, 'JavaExtractor.App',
               '--max_path_length', str(args.max_path_length), '--max_path_width', str(args.max_path_width),
               '--dir', dir, '--num_threads', str(args.num_threads)]

    # print command
    # os.system(command)
    kill = lambda process: process.kill()
    outputFileName = TMP_DIR + prefix + dir.split('/')[-1]
    failed = False
    with open(outputFileName, 'a') as outputFile:
        sleeper = subprocess.Popen(command, stdout=outputFile, stderr=subprocess.PIPE)
        timer = Timer(600000, kill, [sleeper])

        try:
            timer.start()
            stdout, stderr = sleeper.communicate()
        finally:
            timer.cancel()

        if sleeper.poll() == 0:
            if len(stderr) > 0:
                print(sys.stderr, stderr, file=sys.stdout)
        else:
            print(sys.stderr, 'dir: ' + str(dir) + ' was not completed in time', file=sys.stdout)
            failed = True
            subdirs = get_immediate_subdirectories(dir)
            for subdir in subdirs:
                ExtractFeaturesForDir(args, subdir, prefix + dir.split('/')[-1] + '_')
    if failed:
        if os.path.exists(outputFileName):
            os.remove(outputFileName)

TMP_DIR = ""

def ExtractFeaturesForDirsList(args, dirs):
    global TMP_DIR
    TMP_DIR = "./tmp/feature_extractor%d/" % (os.getpid())
    if os.path.exists(TMP_DIR):
        shutil.rmtree(TMP_DIR, ignore_errors=True)
    os.makedirs(TMP_DIR)
    try:
        p = multiprocessing.Pool(4)
        p.starmap(ParallelExtractDir, zip(itertools.repeat(args), dirs))
        #for dir in dirs:
        #    ExtractFeaturesForDir(args, dir, '')
        output_files = os.listdir(TMP_DIR)
        for f in output_files:
            os.system("cat %s/%s" % (TMP_DIR, f))
    finally:
        shutil.rmtree(TMP_DIR, ignore_errors=True)

def get_immediate_subdirectories(a_dir):
    return [(os.path.join(a_dir, name)) for name in os.listdir(a_dir)
            if os.path.isdir(os.path.join(a_dir, name))]


def insert_misconceptions(root):

    ## randomy insert misconception
    if random.random() > 0.5:
        was_replaced = replace_element(root, to_replace="controls_if", replacement="controls_while")
        if was_replaced:
            label = 1
        else:
            label = 0
    else:
        label = 0
    return label


    # if random.random() > 0.5:
    #     was_replaced = replace_element(root, to_replace="controls_if", replacement="controls_while")
    #     if was_replaced:
    #         return 1
    #     else:
    #         return 0
    # else:
    #     return 0


    was_replaced, unchanged_xml = replace_elements_returncopy(root, to_replace="controls_if", replacement="controls_while")
    if was_replaced:
        return True, unchanged_xml
    else:
        return False, None

def insert_random_misconception(root):

    # 0.5 prob to insert a misconception
    if random.random() > 0.5:

        # 0.5 prob for either misconception
        if random.random() > 0.5:
            was_replaced = replace_element(root, to_replace="controls_if", replacement="controls_while")
            if was_replaced:
                return 1,'if_to_while'

        was_replaced = replace_element(root, to_replace="controls_while", replacement="controls_if")
        if was_replaced:
            return 1, 'while_to_if'
        else:
            was_replaced = replace_element(root, to_replace="controls_if", replacement="controls_while")
            if was_replaced:
                return 1, 'if_to_while'

    return 0, "not_changed"

def insert_different_misconceptions(root):
    if random.random() > 0.5:

        # 0.5 prob for either misconception
        if random.random() > 0.5:
            was_replaced = replace_element(root, to_replace="controls_if", replacement="controls_while")
            if was_replaced:
                return 1, 'if_to_while'

        was_replaced = replace_element(root, to_replace="controls_while", replacement="controls_if")
        if was_replaced:
            return 2, 'while_to_if'
        else:
            was_replaced = replace_element(root, to_replace="controls_if", replacement="controls_while")
            if was_replaced:
                return 1, 'if_to_while'

    return 0, "not_changed"

def insert_both_misconception(root):
    type_inserted = ""
    label = 0

    # 0.5 prob to insert a misconception
    if random.random() > 0.5:

        was_replaced = replace_element(root, to_replace="controls_if", replacement="controls_while")
        if was_replaced:
            label = 1
            type_inserted += "if_to_while"

        was_replaced = replace_element(root, to_replace="controls_while", replacement="controls_if")
        if was_replaced:
            label = 1
            type_inserted += 'while_to_if'

    if len(type_inserted) > 13:
        type_inserted = 'both'

    return label, type_inserted




def get_projects_dict_representation(projects_path):
    labels = []
    all_parents_dict = []
    all_terminal_nodes = []
    roots = []
    insertion_count = {'if_to_while':0,
                       'while_to_if':0,
                       'both':0}

    for users_chunk_folder in os.listdir(projects_path):
        print("Now in chunk ", users_chunk_folder)

        if users_chunk_folder == ".DS_Store":
            continue

        users_chunk_path = os.path.join(projects_path, users_chunk_folder)
        for index, user_folder in enumerate(os.listdir(users_chunk_path)):

            if user_folder == ".DS_Store":
                continue

            if index % 50 == 0:
                print("In user number: ", str(index))

            user_path = os.path.join(users_chunk_path, user_folder)

            for aia_project in os.listdir(user_path):
                absProjDir = os.path.join(user_path, aia_project)

                if aia_project == ".DS_Store":
                    continue

                try:
                    root = proj_to_xml(absProjDir)
                except BadZipfile:
                    print("not a zipfile in path: ")
                    print(absProjDir)
                    continue
                # empty project
                if root is None:
                    continue

                root = arrange_xml(root)
                ## at the moment running it three times, to take care of new childs, TODO: find optimal solution
                root = arrange_xml(root)
                root = arrange_xml(root)

                dot = xml_to_dot(root)
                label = insert_misconceptions(root)
                # label, type_inserted = insert_random_misconception(root)
                # label, type_inserted = insert_both_misconception(root)
                # label, type_inserted = insert_different_misconceptions(root)

                # if type_inserted in insertion_count:
                #     insertion_count[type_inserted] += 1

                # label = insert_misconceptions(root)
                #
                #
                #
                parents_path_map, terminal_nodes = xml_to_paths_map(root, 8)
                all_terminal_nodes.append(terminal_nodes)
                all_parents_dict.append(parents_path_map)
                roots.append(root)
                labels.append(label)
                # labels.append(1)



                ### for appending only changed projects
                # was_replaced, origin_xml = insert_misconceptions(root)
                # if was_replaced:
                #     changed_parents_path_map, changed_terminal_nodes = xml_to_paths_map(root, 8)
                #     unchanged_parents_path_map, unchanged_terminal_nodes = xml_to_paths_map(origin_xml, 8)
                #
                #     all_terminal_nodes.append(changed_terminal_nodes)
                #     all_terminal_nodes.append(unchanged_terminal_nodes)
                #
                #     all_parents_dict.append(changed_parents_path_map)
                #     all_parents_dict.append(unchanged_parents_path_map)
                #
                #     labels.append(1)
                #     roots.append(root)
                #     labels.append(0)
                #     roots.append(origin_xml)
                #
                # else:
                #     continue
    print(insertion_count)
    return all_parents_dict, all_terminal_nodes, labels, roots

if __name__ == '__main__':

    parser = ArgumentParser()
    parser.add_argument("-maxlen", "--max_path_length", dest="max_path_length", required=False, default=8)
    parser.add_argument("-maxwidth", "--max_path_width", dest="max_path_width", required=False, default=2)
    parser.add_argument("-threads", "--num_threads", dest="num_threads", required=False, default=64)
    # parser.add_argument("-j", "--jar", dest="jar", required=True)
    parser.add_argument("-dir", "--dir", dest="dir", required=False)
    parser.add_argument("-file", "--file", dest="file", required=False)
    args = parser.parse_args()

    # if args.file is not None:
    #     command = 'java -cp ' + args.jar + ' JavaExtractor.App --max_path_length ' + \
    #               str(args.max_path_length) + ' --max_path_width ' + str(args.max_path_width) + ' --file ' + args.file
    #     os.system(command)
    # elif args.dir is not None:
    #     subdirs = get_immediate_subdirectories(args.dir)
    #     to_extract = subdirs
    #     if len(subdirs) == 0:
    #         to_extract = [args.dir.rstrip('/')]
    #     ExtractFeaturesForDirsList(args, to_extract)

    print(sys.getrecursionlimit())
    sys.setrecursionlimit(2500)

    top_paths = pickle.load(open("../data/top_paths.pkl", "rb"))
    root_for_paths = pickle.load(open("../data/top_paths_root.pkl", "rb"))
    #
    dot = visualize_attention_test(root_for_paths, top_paths)

    # all_parents_dict, all_terminal_nodes, labels, roots = get_projects_dict_representation("../data/insertion_test_projects")
    all_parents_dict, all_terminal_nodes, labels, roots = get_projects_dict_representation("../data/misconceptions")
    # all_parents_dict, all_terminal_nodes, labels = get_projects_dict_representation("../data/misconceptions")
    pickle.dump(roots[0], open("../data/top_paths_root.pkl", "wb"))


    to_write = get_code2vec_inputfile(labels, all_parents_dict, all_terminal_nodes)
    num_examples = len(labels)
    print("out of: " + str(num_examples) + ", " + str(labels.count(1)) + " were inserted with misconception")
    with open(args.file, "a+") as text_file:
        to_write.rstrip('\n')
        text_file.write(to_write)

    # main(args)
