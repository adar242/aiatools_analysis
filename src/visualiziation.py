from graphviz import Digraph
from re import escape
import matplotlib.pyplot as plt
from parser import  pathstring_to_list, xml_to_paths_map, get_clean_terminal_path




def xml_to_dot(xml_root, project_name="default_name"):

    parent_map = {}
    for index, p in enumerate(xml_root.iter()):
        p.attrib["key"] = p.tag + str(index)
        for c in p:
            parent_map[c] = p

    dot = Digraph(comment=project_name)
    for child in parent_map.keys():
        # dot.node(escape(child.attrib["key"]), child.tag)
        dot.node(escape(child.attrib["key"]), child.attrib["key"])

        parent = parent_map[child]

        # dot.node(escape(parent.attrib["key"]), parent.tag)
        # dot.edge(escape(parent.attrib["key"]), escape(child.attrib["key"]), color='green')
        dot.node(escape(parent.attrib["key"]), parent.attrib["key"])
        dot.edge(escape(parent.attrib["key"]), escape(child.attrib["key"]), color='green')
    return dot

def blockrep_to_dot(block_representation):
    dot = Digraph()
    for node in block_representation[2]:
        dot.node(escape(node.attrib["key"]), node.tag)

    for node_index, node_children in enumerate(block_representation[1]):
        for child_index in node_children:

            dot.edge(escape(block_representation[2][node_index].attrib['key']), escape(block_representation[2][child_index].attrib['key']))
    return dot

def visualize_attention(xml_root, top_paths, project_name = "attention tree"):
    """
    takes an xml representation of a project and a list of its top paths
    returns a new dot representation with the top paths colored.


    top_paths is an ordered list of dicts, where each dict has 4 keys: score (prob float), path (path string in c2v
    format, token1 and token2 (strings, the first and last word for the path)
    """
    parent_map = {}
    for index, p in enumerate(xml_root.iter()):
        p.attrib["key"] = p.tag + str(index)
        for c in p:
            parent_map[c] = p

    dot = Digraph(comment=project_name)
    for child in parent_map.keys():
        dot.node(escape(child.attrib["key"]), child.tag)

        parent = parent_map[child]

        dot.node(escape(parent.attrib["key"]), parent.tag)
        dot.edge(escape(parent.attrib["key"]), escape(child.attrib["key"]))

    top_path_dict = top_paths[1]
    first_token = top_path_dict["token1"]
    last_token = top_path_dict["token2"]
    path_as_list = pathstring_to_list(top_path_dict['path'])

    for child in parent_map.keys():
        if child.tag == first_token:
            parent = parent_map[child]
            dot.edge(escape(parent.attrib["key"]), escape(child.attrib["key"]), color ='green')
            new_child = parent
            for element in path_as_list[1::2]:
                if element == '^':
                    parent = parent_map[new_child]
                    dot.edge(escape(parent.attrib["key"]), escape(new_child.attrib["key"]), color='green')
                    new_child = parent

                elif element == '_':
                    parent = new_child
                    for child in parent_map.keys():
                        if parent_map[child] == parent:
                            dot.edge(escape(parent.attrib["key"]), escape(child.attrib["key"]), color='green')
                            parent = child
                            break


    return dot

def visualize_attention_test(xml_root, top_paths, project_name = "attention tree"):
    """
    takes an xml representation of a project and a list of its top paths
    returns a new dot representation with the top paths colored.


    top_paths is an ordered list of dicts, where each dict has 4 keys: score (prob float), path (path string in c2v
    format, token1 and token2 (strings, the first and last word for the path)
    """
    parent_map = {}
    for index, p in enumerate(xml_root.iter()):
        p.attrib["key"] = p.tag + str(index)
        for c in p:
            parent_map[c] = p

    paths_map, terminal_nodes = xml_to_paths_map(xml_root, 8)

    dot = Digraph(comment=project_name)
    for child in parent_map.keys():
        dot.node(escape(child.attrib["key"]), child.tag)

        parent = parent_map[child]

        dot.node(escape(parent.attrib["key"]), parent.tag)
        dot.edge(escape(parent.attrib["key"]), escape(child.attrib["key"]))

    top_path_dict = top_paths[2]
    first_token = top_path_dict["token1"]
    last_token = top_path_dict["token2"]
    path_as_list = pathstring_to_list(top_path_dict['path'])
    clean_path_as_list = path_as_list[::2]

    # dot.edge(escape(parent.attrib["key"]), escape(child.attrib["key"]), color='green')
    first_node_candidates = [element for element in terminal_nodes if element.tag == first_token]
    last_node_candidates = [element for element in terminal_nodes if element.tag == last_token]

    for first_node_option in first_node_candidates:
        first_node_parents = paths_map[first_node_option][1]

        for last_node_option in last_node_candidates:
            last_node_parents = paths_map[last_node_option][1]

            # TODO: right here is the problem, need to make sure the returned path is computed correctly,
            #  and also that it matches the original path
            terminal_path = get_clean_terminal_path(first_node_option, last_node_option, first_node_parents, last_node_parents,8)
            if not terminal_path:
                continue

            # this is the right path, color the right edges
            else:
                terminal_path_tags = [element.tag for element in terminal_path[1:-1]]
                if terminal_path_tags != clean_path_as_list:
                    continue


                for i in range(len(terminal_path) - 1):
                    dot.edge(escape(terminal_path[i].attrib['key']), escape(terminal_path[i+1].attrib["key"]), color="green")
                return dot



    return dot


def trim_dict(dict):
    keys_to_del = []

    for key in dict.keys():
        if key > 30:
            keys_to_del.append(key)

    for key in keys_to_del:
        print (key, dict[key])
        del dict[key]
    print(sum(dict.values()), max(dict.keys()))


def plot_dict_as_histogram(dict, clean_dict = True):

    if clean_dict:
        trim_dict(dict)

    plt.bar(dict.keys(), dict.values(),width=.5, color='g')
    plt.title("Number of projects per user histogram")
    plt.xlabel("Number of projects")
    plt.ylabel("Number of users")
    plt.savefig("num_projects_histogram")

    plt.show()
