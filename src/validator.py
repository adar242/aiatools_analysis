import pandas as pd
import CONFIG
import os
from xml.etree.ElementTree import ParseError
from aiatools import *
from parser import *





min_blocks = 20
max_blocks = 100
min_comps = 3
max_comps = 20
min_num_projects = 3


def validate_csv():
    df = pd.read_csv("100_embeddings.csv")
    print("len of csv is:", len(df))
    print("sum of label col is", sum(df['labels']))

def validate_projects():
    user_counter = 0
    errors_counter = 0

    users_dir = CONFIG.users_10k_Dir
    chunks_list = os.listdir(users_dir)
    for user_chunk_dir in chunks_list:

        users_chunk_path = os.path.join(users_dir, user_chunk_dir)
        print("now in chunk: " + str(user_chunk_dir))

        for user in os.listdir(users_chunk_path):

            if int(user_counter) % 50 == 0:
                print("now in user: " + str(user_counter))
            user_counter += 1

            absUserDir = os.path.join(users_chunk_path, user)

            for aia_project in os.listdir(absUserDir):

                absProjDir = os.path.join(absUserDir, aia_project)

                try:
                    aia = AIAFile(absProjDir)
                except (TypeError, KeyError, ParseError) as e:
                    errors_counter += 1
                    print(e)
                    continue


                if validate_project(aia, user):
                    xml_root = proj_to_xml(absProjDir)

                    ##at the moment running it twice, to take care of new childs, TODO: find optimal solution
                    root = arrange_xml(root)
                    root = arrange_xml(root)
                    parents_dict = xml_to_parents_map(root)

                    replace_element(parents_dict, to_replace="if_statement", replacement='while_statement')
                    x = 1

def validate_project(aia, user_key):

    if not valid_max_blocks(aia,max_blocks):
        return False
    if not valid_min_blocks(aia, min_blocks):
        return False
    if not valid_min_comps(aia, min_comps):
        return False
    if not valid_max_comps(aia, max_comps):
        return False
    if not valid_user_min_projects(user_key, min_num_projects):
        return False
    if not valid_if_statement(aia):
        return False

def valid_min_blocks(aia, min_blocks):
    if aia.blocks.count() < min_blocks:
        return False
    return True

def valid_max_blocks(aia, max_blocks):
    if aia.blocks.count() > max_blocks:
        return False
    return True

def valid_if_statement(aia):
    pass

def valid_min_comps(aia, min_comps):
    if aia.components.count() < min_comps:
        return False
    return True

def valid_max_comps(aia, max_comps):
    if aia.components.count() > max_comps:
        return False
    return True

def valid_user_min_projects(user_key, min_projects):
    user_key = int(user_key)
    users_df = pd.read_csv(CONFIG.users_10k_dataframe_path)

    user_data = users_df.loc[users_df['user_key'] == str(int(user_key))]
    if user_data["num_projects"] < min_projects:
        return False
    return True

if __name__ == '__main__':
    validate_csv()