from sklearn.cluster import KMeans
import matplotlib.pyplot as plt
import seaborn as sn
import pandas as pd
from scipy import stats


def k_means(dataset, k=5):
    distortions = []
    for i in range(1, 11):
        km = KMeans(
            n_clusters=i, init='random',
            n_init=10, max_iter=300,
            tol=1e-04, random_state=0
        )
        km.fit(dataset)
        distortions.append(km.inertia_)

    # plot
    plt.plot(range(1, 11), distortions, marker='o')
    plt.xlabel('Number of clusters')
    plt.ylabel('Distortion')
    plt.show()

def get_corr_matrix(df):
    df_corr = pd.DataFrame()  # Correlation matrix
    df_p = pd.DataFrame()  # Matrix of p-values
    for x in df.columns:
        for y in df.columns:
            corr = stats.pearsonr(df[x], df[y])
            df_corr.loc[x, y] = corr[0]
            df_p.loc[x, y] = corr[1]

    return df_p, df_corr

def get_significant_corrs(corr_df, p_df):
    signif_df = pd.DataFrame()
    for col_index_1 in corr_df.columns:
        for col_index_2 in corr_df.columns:

            if corr_df.loc[col_index_1, col_index_2] > 0.5:
                corr = corr_df.loc[col_index_1, col_index_2]
                signif_df.loc[col_index_1,col_index_2] = corr

    return signif_df

def plot_corr_heatmap(df_corr):
    sn.heatmap(df_corr, annot=True)
    plt.show()
