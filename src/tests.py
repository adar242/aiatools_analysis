

def validate_code2vec_input(file_path):
    """
    Checks the given file in file_path is formatted according to code2vec, IE:
    1. each row is an example, each example is space-delimited list of fields
    2. the first field is the target label (in our case, a number in range (1,k) where k is the number of clusters used)
    3. each field after the first one is a context.
    4. each context have 3 components separated by commas: token,path,token
    5. each of those components cannot contain commas or spaces
    """

    labels = []

    with open(file_path) as fp:

        while True:
            try:
                line = fp.readline()
                example_row = line.split(sep=" ")

                assert line != "", "line cant be empty"
                target_label = int(example_row[0])
                labels_range = list(range(0,200))
                assert target_label in labels_range, "target label should be number in (1,200)" + str(example_row)

                labels.append(example_row[0])

                for context in example_row[1:]:
                    context_comps = context.split(sep=',')

                    assert len(context_comps) == 3 or context_comps == ["\n"], "length of context comps should be exactly 3" + str(context_comps)

                if not line:
                    break
            except Exception as e:
                pass

    return set(labels)



def main():


    test_input_path = "../results/appinventor_200kmean_4length_withoutnames.train.raw copy.txt"
    code2vec_input_path = "../results/appinventor_200kmean_8length_withnames.train.raw.txt"

    print(validate_code2vec_input(code2vec_input_path))



if __name__ == "__main__":
    main()