from aiatools import *
import os
from statistics import mean
import numpy as np
import pandas as pd
from analysis import *
from xml.etree.ElementTree import ParseError
from parser import *
from visualiziation import *
import CONFIG
from zipfile import BadZipfile
from ESN import AppInventorEncoder
import sys

from sklearn.ensemble import GradientBoostingClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeRegressor
from sklearn.svm import LinearSVC
from sklearn.dummy import DummyClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import f1_score

from sklearn.model_selection import cross_val_score
from sklearn.model_selection import ShuffleSplit



from sklearn.metrics import classification_report



n_iterations = 1000




users_10k_Dir = CONFIG.users_10k_Dir
project_10k_dataframe_path = CONFIG.project_10k_dataframe_path

users_46k_Dir = CONFIG.users_10k_dataframe_path
dataframe_46k_path = CONFIG.dataframe_46k_path

users_10k_dataframe_path = CONFIG.users_10k_dataframe_path

misconceptions_projects_dir_path = CONFIG.misconceptions_projects_dir_path

labeled_100_projects_path = r"../untouched_100_projects"


projects_table_column_names = ["user_key", "project_key", "num_screens", "num_components", "avg_comps_per_screen",
                               "num_blocks", "avg_blocks_per_screen"]

users_table_column_names = ["user_key", "num_projects", "screen_sum", "avg_screens_per_project",
                            "comps_sum", "avg_components_per_project", "blocks_sum",
                            "avg_blocks_per_project"]

def main():
    logging.basicConfig(level=logging.ERROR)

    users_dir = users_10k_Dir
    chunks_list = os.listdir(users_dir)

    alphabet = set([])
    projects = []
    y = []
    categs = []
    project_names = []

    # for user_chunk_dir in chunks_list:
    #
    #     users_chunk_path = os.path.join(users_dir, user_chunk_dir)
    #     print("now in chunk: " + str(user_chunk_dir))
    #
    #     for user in os.listdir(users_chunk_path):
    #
    #         absUserDir = os.path.join(users_chunk_path, user)
    for category_folder in os.listdir(labeled_100_projects_path):
        for aia_project in os.listdir(os.path.join(labeled_100_projects_path,category_folder)):

            absProjDir = os.path.join(labeled_100_projects_path, category_folder,aia_project)
            # absProjDir = '../data/misconceptions/if_while_misconception.aia'

            try:
                root = proj_to_xml(absProjDir)
            except BadZipfile:
                print("not a zipfile in path: ")
                print(absProjDir)
                continue

            if root is None:
                continue

            root = arrange_xml(root)
            ##at the moment running it twice, to take care of new childs, TODO: find optimal solution
            root = arrange_xml(root)
            root = arrange_xml(root)
            parents_dict = xml_to_parents_map(root)

            dot = xml_to_dot(root)

            nodes_list = parentmap_to_nodeslist(parents_dict, root)
            # adj_matrix = create_adj_matrix(nodes_list, parents_dict)

            tags_set = nodes_to_tags_set(nodes_list)

            alphabet = alphabet.union(tags_set)

            projects.append((nodes_to_tags_list(nodes_list),create_adj_matrix(nodes_list, parents_dict)))
            print(len(nodes_list))
            categs.append(category_folder)
            project_names.append(aia_project[:-3])

            if 'positive' in aia_project:
                y.append(1)
            else:
                y.append(-1)

    # y = pd.Series(y)

    try:
        root = proj_to_xml("../Pong_MASTER.aia")
    except BadZipfile:
        print("not a zipfile in path: ")
        print(absProjDir)

    if not root is None:

        root = arrange_xml(root)
        ##at the moment running it twice, to take care of new childs, TODO: find optimal solution
        root = arrange_xml(root)
        root = arrange_xml(root)

        # parents_dict = xml_to_parents_map(root)
        parents_dicts_list = xml_to_parents_dict_list(root)

        # dot = xml_to_dot(root)

        for parents_dict in parents_dicts_list:

            nodes_list = parentmap_to_nodeslist(parents_dict, root)
            # adj_matrix = create_adj_matrix(nodes_list, parents_dict)

            tags_set = nodes_to_tags_set(nodes_list)

            alphabet = alphabet.union(tags_set)

            projects.append((nodes_to_tags_list(nodes_list), create_adj_matrix(nodes_list, parents_dict)))


        categs.append("pong_untouched")
        project_names.append("Pong_MASTER")

    embedder = AppInventorEncoder(alphabet)

    encodings = []

    for project in projects:
        encoding = embedder.encode([project])
        encodings.append(encoding)



    X = pd.DataFrame(encodings)

    # X['labels'] = y
    X['category'] = categs
    X['project_name'] = project_names



    X.to_csv("101_untouched_embeddings.csv", index=False)

    # X = pd.read_csv('100_embeddings_dim_256.csv', index_col=None)
    # y = X['labels']
    # X = X.drop(columns=['labels'])
    #
    # svc_model = LinearSVC()
    # svc_model.max_iter = 7600
    # cv = ShuffleSplit(n_splits=10000, test_size=0.2, random_state=0)
    # svc_scores = np.average(cross_val_score(svc_model, X, y, cv=cv))
    # random_scores = np.average(cross_val_score(DummyClassifier(),X, y, cv=cv))
    #
    # print(svc_scores)
    # print(random_scores)

    # training_X, testing_X, training_y, testing_y = ordered_split(X, y, 70)
    # # training_X, testing_X, training_y, testing_y = random_split(X, 75)
    #
    # matrices = get_confusion_matrices(training_X, training_y, testing_X, testing_y)
    # models_accuracy = get_f1_score(training_X, testing_X, training_y, testing_y)
    # for model, accuracy in models_accuracy.items():
    #     print(model, accuracy)





    # str_to_print = "['"
    # for letter in alphabet:
    #     str_to_print += letter + "','"
    #
    # with open("Output.txt", "w") as text_file:
    #     text_file.write(str_to_print)







                # dot = xml_to_dot(root)

                # if was_replaced:
                #     file_path = "../results/if_while_replacement_dots/" + aia_project[:-3] + "txt"
                #     f = open(file_path, "w+")
                #     f.write(dot.source)



    #
    #             nodes = get_terminal_nodes(root)
    #             paths = parse_paths(nodes, root)
    #             for path in paths:
    #                 parse_context_path(path)
    #             x = 1


    # users_df = pd.read_csv(users_10k_dataframe_path)
    # projects_df = pd.read_csv(project_10k_dataframe_path)
    #
    # cols_to_delete = [col for col in projects_df.columns if (col[-8:] == "y_blocks" or col[-8:] == "e_blocks")]
    #
    # merged_df = pd.merge(projects_df.drop(labels=cols_to_delete, axis=1),users_df,on="user_key")
    # df_pvalues, df_corr = get_corr_matrix(merged_df.drop(labels=["user_key", "project_key"], axis=1))
    # signif_corr_df = get_significant_corrs(df_corr, df_pvalues)
    # num_nan_values = signif_corr_df.isna().sum().sum()
    # print("num of nan values (not signif corrs) is:" + str(num_nan_values) + " out of total: " + str(df_corr.count().sum()))
    #
    #
    # nlargest = signif_corr_df.nlargest(5,signif_corr_df.columns)
    # cols_to_delete = [col for col in nlargest.columns if col not in nlargest.index.values]
    # nlargest = nlargest.drop(labels=cols_to_delete, axis=1)
    # plot_corr_heatmap(nlargest)


    # df_pvalues, df_corr = plot_corr_matrix(df.drop(labels="user_key",axis=1))



    # df = pd.DataFrame()
    # create_dataframe(df, users_dir=users_10k_Dir)

    # df = pd.read_csv(filepath_or_buffer=dataframe_10k_path)
    # columns_sums = df.sum()
    # plot_freqs(columns_sums.drop("project_key"))

    # slim_df = df[project_table_column_names]
    #
    # x = slim_df.describe()
    # print(x)

    # num_screens_avg = df["num_screens"].mean
    # num_screens_std = df["num_screens"].std
    # num_components_avg = df["num_components"].mean
    # num_components_std = df["num_components"].std
    # num_blocks_avg = df["num_blocks"].mean
    # num_blocks_std = df["num_blocks"].std

def get_projects_histogram_dict(users_df_path):
    users_df = pd.read_csv(users_df_path)

    # key is num of projects and value is num of users with [key] number of projects
    projects_histogram_dict = {}

    for index, row in users_df.iterrows():
        num_projects = row['num_projects']

        if num_projects not in projects_histogram_dict.keys():
            projects_histogram_dict[num_projects] = 1
        else:
            projects_histogram_dict[num_projects] += 1

    return projects_histogram_dict



def create_users_dataframe(df,users_dir):
    user_counter = 0
    errors_counter = 0
    no_projects_counter = 0
    errored_user_counter = 0

    chunks_list = os.listdir(users_dir)
    for user_chunk_dir in chunks_list:

        users_chunk_path = os.path.join(users_dir, user_chunk_dir)
        print("now in chunk: " + str(user_chunk_dir))

        for user in os.listdir(users_chunk_path):
            user_errors = 0

            if int(user_counter) % 50 == 0:
                print("now in user: " + str(user_counter))
            user_counter += 1

            absUserDir = os.path.join(users_chunk_path, user)

            row_dict = {
                "user_key": user,
                "num_projects": 0,
                "screen_sum": 0,
                "avg_screens_per_project": 0,
                "comps_sum": 0,
                "avg_components_per_project": 0,
                "blocks_sum": 0,
                "avg_blocks_per_project": 0,
            }

            for aia_project in os.listdir(absUserDir):

                absProjDir = os.path.join(absUserDir, aia_project)

                try:
                    aia = AIAFile(absProjDir)
                except (TypeError, KeyError, ParseError) as e:
                    errors_counter += 1
                    user_errors += 1
                    print(e)
                    continue

                row_dict["num_projects"] += 1
                row_dict["screen_sum"] += aia.screens.count()
                row_dict["comps_sum"] += aia.components.count()
                row_dict["blocks_sum"] += aia.blocks.count()

            num_projects = row_dict["num_projects"]
            if num_projects == 0 and user_errors == 0:
                # print("user: " + str(user) + " have no projects, skipping")
                no_projects_counter += 1
                continue
            elif not user_errors == 0:
                errored_user_counter += 1
                continue

            row_dict["avg_screens_per_project"] = row_dict["screen_sum"] / num_projects
            row_dict["avg_components_per_project"] = row_dict["comps_sum"] / num_projects
            row_dict["avg_blocks_per_project"] = row_dict["blocks_sum"] / num_projects

            df = df.append(row_dict, ignore_index=True)

    df.fillna(0, inplace=True)
    df.to_csv(path_or_buf=users_10k_dataframe_path, index=False, header=True)
    print('\n\n\n total number of users is: ' + str(user_counter))
    print('\n\n\n total number of errors is: ' + str(errors_counter))
    print("length of df is: " + str(len(df.index)))
    print("num of users without project is: " + str(no_projects_counter))
    print("num of users without project due to errors in project is: " + str(errored_user_counter))

def plot_freqs(columns_sums):
    comps_freq_dict = {}

    comps_types_columns = pd.Series()
    for column_name, value in columns_sums.items():
        if column_name[-5:] == "comps":
            comps_types_columns = comps_types_columns.append(pd.Series(columns_sums[column_name]))
    comps_count_avg = comps_types_columns.mean()

    for column_name, value in columns_sums.items():
        if column_name[-5:] == "comps":
            if value > comps_count_avg:
                comps_freq_dict[column_name[4:-11]] = value

    plot_frequency(comps_freq_dict,title="Components types frequency",
                   xlabel="Components types with over the average amount of occurrences",ylabel="Occurrences")

    block_category_freq_dict = {}
    for column_name, value in columns_sums.items():
        if column_name[-8:] == "y_blocks":
            block_category_freq_dict[column_name[4:-16]] = value
    plot_frequency(block_category_freq_dict, title="Blocks categories frequencies",
                   xlabel="Block categories", ylabel="Occurrences")


def plot_frequency(freq_dict, title, xlabel, ylabel):

    plt.figure(num=None, figsize=(16, 8), dpi=80, facecolor='w', edgecolor='k')
    plt.bar(freq_dict.keys(), freq_dict.values(), align='center')
    plt.title(title)
    plt.xlabel(xlabel)
    plt.ylabel(ylabel)
    plt.tight_layout()

    plt.show()


def create_dataframe(df,users_dir):
    user_counter = 0
    errors_counter = 0

    chunks_list = os.listdir(users_dir)
    for user_chunk_dir in chunks_list:

        users_chunk_path = os.path.join(users_dir, user_chunk_dir)
        print("now in chunk: " + str(user_chunk_dir))

        for user in os.listdir(users_chunk_path):

            if int(user_counter) % 50 == 0:
                print("now in user: " + str(user_counter))
            user_counter += 1


            absUserDir = os.path.join(users_chunk_path, user)

            for aia_project in os.listdir(absUserDir):

                absProjDir = os.path.join(absUserDir, aia_project)

                try:
                    aia = AIAFile(absProjDir)
                except (TypeError, KeyError, ParseError) as e:
                    errors_counter += 1
                    print(e)
                    continue

                blocks_by_category_dict = aia.blocks().count(group_by=category)
                dict_keys = list(blocks_by_category_dict.keys())
                for key in dict_keys:
                    if key == "Components":
                        x = 1
                    blocks_by_category_dict["num_" + key + "_category_blocks"] = blocks_by_category_dict.pop(key)

                blocks_by_type_dict = aia.blocks().count(group_by=type)
                dict_keys = list(blocks_by_type_dict.keys())
                for key in dict_keys:
                    blocks_by_type_dict["num_" + key + "_type_blocks"] = blocks_by_type_dict.pop(key)

                comps_by_type_dict = aia.components().count(group_by=type)
                dict_keys = list(comps_by_type_dict.keys())
                for key in dict_keys:
                    comps_by_type_dict["num_" + key.name + "_type_comps"] = comps_by_type_dict.pop(key)

                row_dict = {
                    "user_key": user,
                    "project_key": aia_project,
                    "num_screens": aia.screens.count(),
                    "num_components": aia.components.count(),
                    "avg_comps_per_screen": mean([screen.components.count() for screen in aia.screens]),
                    "num_blocks": aia.blocks.count(),
                    "avg_blocks_per_screen": mean([screen.blocks.count() for screen in aia.screens]),
                }

                row_dict.update(blocks_by_category_dict)
                row_dict.update(blocks_by_type_dict)
                row_dict.update(comps_by_type_dict)

                df = df.append(row_dict, ignore_index=True)

    df.fillna(0, inplace=True)
    # df.to_csv(path_or_buf=dataframe_46k_path, index=False, header=True)
    print('\n\n\n total number of users is: ' + str(user_counter))
    print('\n\n\n total number of errors is: ' + str(errors_counter))
    print("length of df is: " + str(len(df.index)))


def ordered_split(X, y, percent):
    dividing_index = int((len(y) / 100) * percent)

    training_X = X[:dividing_index]
    testing_X = X[dividing_index:]
    training_y = y[:dividing_index]
    testing_y = y[dividing_index:]
    return training_X, testing_X, training_y, testing_y

def random_split(full_df, ratio):
    df = full_df.sample(frac=1).reset_index(drop=True)

    y = df['labels']
    X = df.drop(columns=['labels'])

    dividing_index = int((len(y) / 100) * ratio)

    training_X = X[:dividing_index]
    testing_X = X[dividing_index:]
    training_y = y[:dividing_index]
    testing_y = y[dividing_index:]
    return training_X, testing_X, training_y, testing_y


# given a dictionary of models and the training data, fit each of the models to the data
def fit_models(models,training_X, training_y):
    for model in models.values():
        model.fit(training_X,training_y)

# given a dictionary of fitted models and testing data,
# return  a dictionary of model names and their predictions
def generate_predictions(models, testing_X):
    predictions = {}
    for model_name in models.keys():
        predictions[model_name] = models[model_name].predict(testing_X)

    return predictions

def print_model_scores(model_predictions, testing_y):

    arrayed_y = testing_y.to_numpy()
    models_accuracy = {}

    for model_name in model_predictions.keys():

        curr_model_predictions = model_predictions[model_name]
        curr_model_right_predicts = 0

        for i in range(len(arrayed_y)):
            if curr_model_predictions[i] == arrayed_y[i]:
                curr_model_right_predicts += 1


        models_accuracy[model_name] = curr_model_right_predicts / len(testing_y)

    # not printing model score as percent of right predictions
    for model_name in models_accuracy.keys():
        print(model_name, "percent of right predictions:", models_accuracy[model_name])

    return models_accuracy

def get_confusion_matrices(training_X, training_y, testing_X, testing_y):
    models_confusion_matrices = {"boosted tree model": np.zeros((2,2)),
                               "decision tree model": np.zeros((2,2)),
                               "linear svc model": np.zeros((2,2)),
                               "logistic regression model": np.zeros((2,2)),
                               "random model": np.zeros((2,2)),
                               }

    models = {
        "boosted tree model": GradientBoostingClassifier(),
        "decision tree model": DecisionTreeRegressor(),
        "linear svc model": LinearSVC(),
        "logistic regression model": LogisticRegression(),
    }

    # fit models
    fit_models(models, training_X, training_y)

    # generate predictions
    model_predictions = generate_predictions(models, testing_X)
    model_predictions["random model"] = np.random.choice([-1, 1], size=(len(testing_y),), p=[1 / 2, 1 / 2])

    # fill out the matrices
    for user_key in models_confusion_matrices.keys():

        for record_index in range(len(testing_y)):

            model_prediction = model_predictions[user_key][record_index]
            ground_truth = testing_y.iloc[record_index]

            # correct hit
            if model_prediction == 1 and ground_truth == 1:
                models_confusion_matrices[user_key][0][0] += 1
                continue

            # correct rejection
            if model_prediction == -1 and ground_truth == -1:
                models_confusion_matrices[user_key][1][1] += 1
                continue

            # miss
            if model_prediction == -1 and ground_truth == 1:
                models_confusion_matrices[user_key][0][1] += 1
                continue

            # false alarm
            if model_prediction == 1 and ground_truth == -1:
                models_confusion_matrices[user_key][1][0] += 1
                continue

    return models_confusion_matrices

def get_f1_score(training_X, testing_X, training_y, testing_y):
    models_average_score = {"boosted tree model": 0,
                               "decision tree model": 0,
                               "linear svc model": 0,
                               "logistic regression model": 0,
                               "random model": 0,
                               }

    for i in range(n_iterations):
        models = {
            "boosted tree model": GradientBoostingClassifier(),
            "decision tree model": DecisionTreeRegressor(),
            "linear svc model": LinearSVC(),
            "logistic regression model": LogisticRegression(),
        }

        models["linear svc model"].max_iter = 7600
        models["logistic regression model"].max_iter = 7600




        # fit models
        fit_models(models, training_X, training_y)

        # generate predictions
        model_predictions = generate_predictions(models, testing_X)
        model_predictions["random model"] = np.random.choice([-1, 1], size=(len(testing_y),), p=[1 / 2, 1 / 2])



        for user_key in model_predictions.keys():
            models_average_score[user_key] += f1_score(testing_y, model_predictions[user_key])

    for user_key in model_predictions.keys():
        models_average_score[user_key] = models_average_score[user_key] / n_iterations


    return models_average_score

def get_average_accuracy(testing_X, testing_y, training_X, training_y):
    models_average_accuracy = {"boosted tree model": [0] * n_iterations,
                               "decision tree model": [0] * n_iterations,
                               "linear svc model": [0] * n_iterations,
                               "logistic regression model": [0] * n_iterations,
                               "random model": [0] * n_iterations,
                               }
    for i in range(n_iterations):

        print("in iteration:", i)
        # specify models
        models = {
            "boosted tree model": GradientBoostingClassifier(),
            "decision tree model": DecisionTreeRegressor(),
            "linear svc model": LinearSVC(),
            "logistic regression model": LogisticRegression(),
        }

        models["linear svc model"].max_iter = 7600
        models["logistic regression model"].max_iter = 7600

        # fit models
        fit_models(models, training_X, training_y)

        # generate predictions
        model_predictions = generate_predictions(models, testing_X)
        model_predictions["random model"] = np.random.choice([-1, 1], size=(len(testing_y),), p=[1 / 2, 1 / 2])

        # print score
        models_accuracy = print_model_scores(model_predictions, testing_y)

        for user_key in models_accuracy.keys():
            models_average_accuracy[user_key][i] = models_accuracy[user_key]

    return models_average_accuracy



if __name__ == "__main__":
    main()
