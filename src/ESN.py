"""
Implements a tree echo state network (Gallicchio and Micheli, 2013) to encode a
scratch project as a vector.

"""

# Copyright (C) 2020
# Benjamin Paaßen
# The University of Sydney

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

__author__ = 'Benjamin Paaßen'
__copyright__ = 'Copyright 2020, Benjamin Paaßen'
__license__ = 'GPLv3'
__version__ = '0.1.0'
__maintainer__ = 'Benjamin Paaßen'
__email__  = 'benjamin.paassen@sydney.edu.au'

import numpy as np

def _initialize_reservoir(dim = 128, sparsity = 0.1, radius = 0.9):
    """ Initializes a reservoir matrix with a sparsity fraction of nonzero
    entries and a spectral radius of radius.

    Parameters
    ----------
    dim: int (default = 128)
        The dimensionality of the reservoir matrix.
    sparsity: float (default = 0.1)
        The sparsity factor; smaller values mean more sparsity.
    radius: float (default = 0.9)
        The spectral radius of the output matrix.

    Returns
    -------
    W: array_like
        A randomly initialized dim x dim reservoir matrix with a sparsity
        fraction of nonzero entries and spectral radius radius.

    """
    W = np.zeros((dim, dim))
    nonzeros = max(1, int(sparsity * dim))
    for i in range(dim):
        # select the nonzero positions at random
        pos = np.random.choice(dim, size=nonzeros, replace = False)
        # initialize randomly
        W[i, pos] = np.random.randn(nonzeros)
    # compute the actual spectral radius
    rho = np.max(np.abs(np.linalg.eigvals(W)))
    # normalize the matrix by that and multiply with the desired spectral
    # radius
    W *= radius / rho
    return W


class AppInventorEncoder:
    """ A tree echo state network to encode appinventor projects as vectors.

    In more detail, this neural network encodes an appinventor project by defining
    for each block type x that could occur in a app inventor project a separate
    function `f_x` and then encoding a tree `x`(`y_1`, ..., `y_k`) recursively
    as follows:

    .. math:: \\phi(x(y_1, \\ldots, y_k)) = f_x(\\phi(y_1), \\ldots, \\phi(y_k))

    In other words, the encoding of a tree is defined as the result of applying
    `f_x` to the encodings of all its children.

    Each function `f_x`, in turn, is defined as a single-layer feedforward
    neural network, i.e. it has the following form.

    .. math:: f_x(\\vec x_1, \\ldots, \\vec x_k) = \\tanh(W_x \\cdot \\sum_{j=1}^k \\vec x_j + \\vec b_x)

    where `W_x`and `b_x` are a weight matrix and a bias vector for the symbol x
    respectively.

    Finally, we employ the tree echo state network architecture, which means
    that we do not learn the parameters `W_x`and `b_x`, but instead we
    initialize them randomly and then keep them fixed (for more details, refer
    to the paper of Gallicchio and Micheli, 2013). The key advantage of this
    approach is that we do not require any training to generate encodings.
    The disadvantage is that the resulting encodings are, to some extent,
    random and interpretation requires care.

    Parameters
    ----------
    dim: int (default = 128)
        The dimensionality of the encoding. This should be set relatively large
        to ensure that even the random coding suffices to capture the necessary
        information. Values at least of 128 are recommended.
    sparsity: float (default = 0.1)
        The sparsity of the weight matrices `W_x`. More precisely, this
        regulates the fraction of nonzero entries in `W_x`.
    radius: float (default = 0.9)
        The spectral radius of the weight matrices `W_x` and the standard
        deviation for the bias vectors `b_x`. Higher values mean more influence
        of values further down the tree, lower values emphasize the root more.

    Attributes
    ----------
    Ws_: dictionary
        A dictionary of symbols to dim x dim weight matrices.
    bs_: dictionary
        A dictionary of symbols to dim-dimensional bias vectors.

    """
    def __init__(self, alphabet_, dim = 128, sparsity = 0.1, radius = 0.9, ):
        # store the parameters in case someone wants to refer to them later
        self.dim = dim
        self.sparsity = sparsity
        self.radius = radius

        # initialize weights and biases
        self.Ws_ = {}
        self.bs_ = {}
        for symbol in alphabet_:
            self.Ws_[symbol] = _initialize_reservoir(dim, sparsity, radius)
            self.bs_[symbol] = np.random.randn(dim) * self.radius

    def add_alphabet(self, alphabet):
        for symbol in alphabet:
            if symbol not in self.Ws_.keys():
                self.Ws_[symbol] = _initialize_reservoir(self.dim, self.sparsity, self.radius)
                self.bs_[symbol] = np.random.randn(self.dim) * self.radius

    def encode(self, project):
        """ Encodes the given app inventor project as a vector.

        Parameters
        ----------
        project: a list of tuples, each tuple represent a tree in the project, such that the first element of the tuple
        is nodes list, and the second element is an adjacency matrice.

        Returns
        -------
        phi: array_like
            a self.dim dimensional vector encoding of the given project.

        """
        phi = np.zeros(self.dim)
        # and then add the encodings for all sprites
        for tree in project:
            phi += self.encode_tree(tree[0], tree[1])
        # return the result
        return phi

    def encode_tree(self, nodes, adj, root = 0):
        """ Encodes the given tree in an app inventor project as a vector.

        Parameters
        ----------
        nodes: list
            A list of block opcodes for each node in the tree.
        adj: list
            The adjacency list of the tree, i.e. adj[i] is the list of
            child indices of node i.
        root: int (default = 0)
            The node at which to start the encoding.

        Returns
        -------
        phi: array_like
            a self.dim dimensional vector encoding of the given project.

        """
        # accumulate the encodings of all children
        phi = np.zeros(self.dim)
        for j in adj[root]:
            phi += self.encode_tree(nodes, adj, j)
        # then apply the parameters for the current node
        x   = nodes[root]
        W_x = self.Ws_[x]
        b_x = self.bs_[x]
        phi = np.tanh(np.dot(W_x, phi) + b_x)
        # return the result
        return phi

# alphabet_ = ['RequestDirectMessages','GotTranslation','EdgeReached','WithinRangeEventEnabled','procedures_callnoreturn','Message','MinimumInterval','BarcodeScanner','Year','LocationChanged','BackgroundImage','ListPicker','XMLTextDecode','DrawLine','DelimiterByte','GetBackgroundPixelColor','2','HomeUrl','Secure','ReadFrom','GotFile','GetBatteryLevel','AfterTimeSet','ClearCaches','mode','logic_false','ShowMessageDialog','Send2ByteNumber','lists_copy','Mentions','ShowChooseDialog','euatgqsa','math_tan','component_component_bmock','KeyFile','Username','HasLongitudeLatitude','ValueStored','GetTags','ZAccel','ReceiveSigned1ByteNumber','FormatDate','TagWritten','NxtColorSensor','TextToSpeech','CurrentUrl','15','4','language-version','BackPressed','math_format_as_decimal','color_black','0','36','CollidedWith','lists_is_list','title','DisplayDropdown','LongitudeFromAddress','SavedRecording','Enabled','ProviderName','Authorize','Pitch','lists_select_item','Spinner','YandexTranslate','32','Form','ExtraValue','Elements','UriEncode','DirectMessages','GetDuration','Play','CanGoBack','FormatTime','math_round','AddressesAndNames','AddSeconds','for_lexical_variable_get','text_trim','AfterSelecting','ReadMode','CollidingWith','component_method','DoScan','text_compare','FullScreen','XML','Completed','Title','TextBox','ColorLeft','method_name','CheckAuthorized','97','Z','BeforePicking','component_set_fet','Rotates','AfterChoosing','Roll','Magnitude','AccelerometerSensor1_Shaking','RequestHeaders','55','MaximumRange','ProviderLocked','AboutScreen','Now','Canvas1_Dragged','UseFront','PirateSprite_Flung','math_subtract','OtherScreenClosed','GoToUrl','NxtDirectCommands','TextAlignment','controls_eval_but_ignore','XAccel','RequestFollowers','AfterPicture','PirateSprite_CollidedWith','Slider','42','ResponseFileName','GetValue','Action','FontBold','Delete','component_set_det','Height','GenerateLight','GotText','PirateSprite_component','lists_position_in','40','ForgetLogin','BottomOfRange','twrszjca','TouchDown','CheckBox1_Changed','Animation','StoreValue','CheckBox','PostTextWithEncoding','PhoneNumberPicker','PositionChanged','AccelerometerSensor','else','ShowFeedback','confounder','TinyDB','ImagePicker','AvailableLanguages','IncomingCallAnswered','Y','Disconnect','Sensitivity','type','TinyWebDB','CharacterEncoding','controls_while','math_number','next','ReceiveSigned2ByteNumber','Url','component_component_block','color_red','VerticalArrangement','PhoneNumberList','Speed','Available','Source','SpeechRecognizer1_GetText','DrawingCanvas_Clear','controls_openAnotherScreen','Flung','WebViewer','Visible','text_changeCase','Interval','FusiontablesControl','component_set_get','BytesAvailableToReceive','controls_forRange','GetBrickName','Timer','Prompt','YAccel','PhoneCall','47','14','StatusChanged','25','DirectMessage','ServiceURL','19','ShowProgressDialog','7','BluetoothServer','arg','local_declaration_expression','list','MoveIntoBounds','IsAccepting','lists_from_csv_row','logic_compare','math_random_float','SoundRecorder','yailtype','AfterSoundRecorded','GetDistance','TouchUp','MakeInstant','83','lists_is_empty','PaintColor','Minute','logic_or','82','setproperty','Setting_OtherScreenClosed','6','ExtraKey','DrawText','value','field','AfterDateSet','math_division','71','DataUri','PostText','18','FollowersReceived','math_random_set_seed','37','Click','Send4ByteNumber','comment','65','MentionsReceived','ShareMessage','lexical_variable_get','TagRead','AddWeeks','10','Open','Latitude','global_declaration','12','math_compare','TextToWrite','60','AfterScan','SpeechRecognizer','BeforeGettingText','getproperty','text_split','ContactPicker','ReceiveUnsignedBytes','SetBackgroundPixelColor','FollowLinks','AddMinutes','ClearTag','RecordVideo','color_dark_gray','ApiKey','ListView','lexical_variable_set','math_convert_angles','BuildRequestData','HasAccuracy','property_name','Query','logic_operation','GetPixelColor','16','SearchResults','Duration','text_contains','Azimuth','GetText','ClearLocations','controls_closeApplication','AccelerationChanged','math_add','SaveFile','LastMessage','LocationSensor','ThumbPosition','Clock1_Now','TableArrangement','color_pink','PutFile','FontItalic','text_replace_all','Start','20','Loop','LaunchPicker','lists_create_with','Resume','component_set_iet','component_ket_get','lists_length','DoQuery','64','MoveForwardIndefinitely','set','ResolveActivity','Get','true','TurnClockwiseIndefinitely','Longitude','Weekday','ResultName','24','AllowCookies','GetFirmwareVersion','logic_negate','text','local_declaration_statement','ClearAll','HorizontalArrangement','number','GotValue','X','component_set_het','MoveBackward','21','math_random_int','Hour','AfterRecording','Follow','Accuracy','WebServiceError','OrientationChanged','ReceiveUnsigned1ByteNumber','ggbnzpii','ActivityPackage','block','YACODEBLOCKS','AfterTextInput','GotFocus','Tweet','lists_to_csv_row','logic_boolean','CurrentPageTitle','81','Vibrate','ColorRight','text_split_at_spaces','GetCurrentProgramName','event_name','ReceiveSignedBytes','text_segment','color_magenta','controls_if','17','color_yellow','DetectColor','SendBytes','MoveForward','Width','ErrorOccurred','SPLITATFIRST','AlignHorizontal','AlignVertical','SelectionIndex','Clear','CurrentAddress','text_length','ConsumerKey','22','text_starts_at','lists_add_items','Canvas1_Flung','ScreenOrientationChanged','ContactName','SendMessage','NxtSoundSensor','text_isEmpty','1','Twitter','MoveTo','RequestMentions','WeekdayName','FontSize','TinyDB1_GetValue','math_divide','LogWarning','TimeInterval','AvailableProviders','controls_closeScreen','SeekTo','26','boolean','53','HtmlTextDecode','PhoneCallEnded','AddDays','Scrollable','TimePicker','math_neg','GetSoundLevel','File','13','Pause','Altitude','Hint','SaveResponse','EmailAddress','BelowRangeEventEnabled','Canvas','PointInDirection','controls_closeScreenWithValue','Initialize','Stop','FriendTimelineReceived','get','GetLightLevel','SearchSuccessful','color_orange','LatitudeFromAddress','IsDevicePaired','math_abs','math_single','MoveBackwardIndefinitely','TurnCounterClockwiseIndefinitely','GotResult','Day','SetDateToDisplay','Selection','DistanceInterval','AfterPicking','controls_forEach','DrawCircle','Ball','AfterGettingText','Shaking','HasAltitude','ImageSprite','Texting','SystemTime','procedures_defnoreturn','collapsed','Language','Catch_Phrases_BeforePicking','27','ShareFile','GoogleVoiceEnabled','controls_choose','SPLIT','math_cos','Save','PictoBoxDeluxe_AfterPicture','Volume','Button','math_ceiling','lists_from_csv_table','eventparam','Picture','color_green','Notifier','Web','Changed','GoForward','mutation','AboveRangeEventEnabled','OrientationSensor','disabled','Camcorder','28','RequestTranslation','GoHome','11','math_trig','35','MakeInstantFromMillis','items','SPLITATANY','lists_lookup_in_pairs','PictoBoxDeluxe_TakePicture','component_event','AfterActivity','LogError','Label','Month','GetRows','52','math_is_a_number','MinValue','AppendToFile','NxtDrive','color_blue','Country','ShareFileWithMessage','SearchTwitter','TopOfRange','Second','NearField','color_make_color','IsPressed','DeAuthorize','Camera','Image','Result','CanGoForward','Touched','PasswordTextBox','Sharing','23','EmailPicker','ConsumerSecret','ElementsFromString','5','color_split_color','NxtLightSensor','lists_to_csv_table','WebViewString','LineWidth','color_gray','DatePicker','ClearCookies','PutTextWithEncoding','RequestFocus','TakePicture','Send1ByteNumber','math_atan2','PutText','TinyWebDB1_GetValue','BelowRange','Canvas1_DrawCircle','NxtTouchSensor','ShowTextDialog','Bounce','lists_replace_item','Clock1_Minute','set_or_get','MaxValue','107','Pressed','lists_is_in','ReceiveSigned4ByteNumber','LogInfo','AboveRange','PirateSprite_Bounce','Clock','controls_do_then_return','GoBack','Sound','math_multiply','TextColor','9','GetRowsWithConditions','ResultUri','xml','BackgroundColor','MultiLine','InsertRow','Distance','ReceiveText','29','80','WithinRange','GetColor','Notifier1_ShowTextDialog','localname','text_join','Connect','SpeechRate','MessageReceived','SendQuery','Speak','vertical_parameters','Clock1_Second','DrawingCanvas_DrawCircle','JsonTextDecode','ColorChanged','lists_insert_item','ActivityClass','DataType','SendText','LostFocus','Text','MakePhoneCall','DrawingCanvas_Dragged','Angle','SaveAs','ShowAlert','color_white','Clock1_Year','GetMillis','Canvas1_Clear','component_set_jet','BluetoothClient','component_type','8','PostFile','30','Class_Names_BeforePicking','color_cyan','41','Notifier1_ShowAlert','math_floor','62','math_on_list','PhoneNumber','procedures_defreturn','math_power','PointTowards','color_light_gray','IsAuthorized','Canvas1_component','statement','ScreenOrientation','TweetWithImage','HideKeyboard','DrawPoint','RequestFriendTimeline','obsufcated_text','PressedEventEnabled','NxtUltrasonicSensor','Checked','controls_closeScreenWithPlainText','3','SetTimeToDisplay','procedures_callreturn','elseif','OpenScreenAnimation','lists_remove_item','ProximityChanged','lists_pick_random_item','DirectMessagesReceived','lists_append_list','Heading','Radius','ProximitySensor','API_Key','NumbersOnly','CloseScreenAnimation','ReceivingEnabled','Dragged','DismissProgressDialog',]


