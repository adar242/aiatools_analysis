from zipfile import ZipFile
import xml.etree.ElementTree as ETree
import re
import copy
from graphviz import Digraph
from re import escape


"""
given an AIA file,
returns an xml representation of that aia file's blocks
"""
def proj_to_xml(aia_proj_path):
    zipped_file = ZipFile(aia_proj_path)

    root = ETree.fromstring("<Master/>")
    for name in zipped_file.namelist():
        if name.startswith('src/') and name.endswith('.bky'):
            blocks_content = zipped_file.open(name, 'r').read().decode('utf-8')

            # screen with no blocks
            if blocks_content == '':
                continue
            xmlstring = re.sub(' xmlns="[^"]+"', '', blocks_content, count=1)
            screen_blocks_root = ETree.fromstring(xmlstring)
            root.append(screen_blocks_root)

    return root

"""
given an xml representation of an aia file's blocks,
returns a list with all the terminal nodes
"""
def get_terminal_nodes(xml_root):
    nodes = []

    for node in xml_root.iter('*'):
        # nodes of length zero are leaf nodes
        if len(node) == 0:
            nodes.append(node)

    return nodes

"""
given an xml representations of an aia files and a list of nodes in that xml tree,
returns paths between each pairwise couple of nodes.
"""




def parse_paths(nodes, xml_root):


    paths = []
    for i in range(len(nodes)):
        first_node = nodes[i]
        first_node_path = find_path(first_node, xml_root)
        for j in range(len(nodes)):

            if i != j:
                second_node = nodes[j]
                second_node_path = find_path(second_node, xml_root)

                if (not first_node_path) or not (second_node_path):
                    continue

                terminal_path = find_terminal_path(first_node_path, second_node_path)
                paths.append(terminal_path)

    return paths


"""
given a node and one of its parents,
returns a list of elements that represent a path from parent to node
"""
def find_path(node, root):
    parent_map = {c:p for p in root.iter() for c in p}

    path = [node]

    if not parent_map.keys():
        return None

    while True:
        try:
            parent = parent_map[node]
            path.append(parent)
            node = parent
        except KeyError as e:
            break

    path.reverse()
    return path


"""
given two paths from a root node to two different terminal nodes,
returns the path between the two terminal nodes
"""


def add_UP_to_path(path):
    new_path = []

    for element in path:
        new_path.append(element)
        new_path.append("^")

    return new_path

def add_DOWN_to_path(path):
    new_path = []

    for element in path:
        new_path.append(element)
        new_path.append("_")
    new_path = new_path[:-1]

    return new_path

def get_terminal_path(first_node, second_node, first_node_parents, second_node_parents, max_len):

    for first_node_index ,node in enumerate(first_node_parents):

        # this will be the first match in the paths, now creating complete path
        second_parents_keys_list = [parent.attrib["key"] for parent in second_node_parents]
        if node.attrib["key"] in second_parents_keys_list:
            second_node_index = second_parents_keys_list.index(node.attrib["key"])

            first_node_cut_path = first_node_parents[:first_node_index]
            second_node_cut_path = second_node_parents[second_node_index:]
            combined_path = [first_node] + first_node_cut_path + second_node_cut_path + [second_node]
            if len(combined_path) > max_len:
                return None
            combined_path = [first_node] + add_UP_to_path(first_node_cut_path) + add_DOWN_to_path(second_node_cut_path) + [second_node]
            return combined_path

    return None

def get_clean_terminal_path(first_node, second_node, first_node_parents, second_node_parents, max_len):

    first_node_parents_copy = copy.deepcopy(first_node_parents)
    first_node_parents_copy.reverse()
    for first_node_index ,node in enumerate(first_node_parents_copy):

        # this will be the first match in the paths, now creating complete path
        second_parents_keys_list = [parent.attrib["key"] for parent in second_node_parents]
        if node.attrib["key"] in second_parents_keys_list:
            second_node_index = second_parents_keys_list.index(node.attrib["key"])

            first_node_cut_path = first_node_parents_copy[:first_node_index]
            second_node_cut_path = second_node_parents[second_node_index:]
            combined_path = [first_node] + first_node_cut_path + second_node_cut_path + [second_node]
            if len(combined_path) > max_len:
                return None
            return combined_path

    return None




def find_terminal_path(first_node, second_node, parents_dict, max_len):

    first_node_parents = [first_node]
    second_node_parents = [second_node]

    curr_first_node_parent = parents_dict[first_node]
    curr_second_node_parent = parents_dict[second_node]

    first_node_done = False
    second_node_done = False

    for i in range(max_len):
        if not first_node_done:
            first_node_parents.append(curr_first_node_parent)
        if not second_node_done:
            second_node_parents.append(curr_second_node_parent)

        # if not reached root node
        if curr_first_node_parent in parents_dict:
            curr_first_node_parent = parents_dict[curr_first_node_parent]
        else:
            first_node_done = True

        # if not reached root node
        if curr_second_node_parent in parents_dict:
            curr_second_node_parent = parents_dict[curr_second_node_parent]
        else:
            second_node_done = True


    for i in range(len(first_node_parents)):
        for j in range(len(second_node_parents)):

            # if equal then this is the common parent for both nodes and where they intersect on their path to root
            if first_node_parents[i] == second_node_parents[j]:
                second_node_cut_path = second_node_parents[:j]
                second_node_cut_path.reverse()
                combined_path = first_node_parents[:i + 1] + second_node_cut_path
                if len(combined_path) > max_len:
                    return None
                return combined_path


def parse_context_path(path):
    for index,element in enumerate(path):
        if element == 'UP' or element == 'DOWN':
            continue
        path[index] = element.tag

"""
given the root of the blocks xml representation
returns modified xml such that the different (specific) attributes will be the child-element of their block, not just attr
"""
def arrange_xml(blocks_xml_root, keep_instance_names = True):

    attrs_to_remove = ['x', 'y', 'id','comment', 'inline','name', 'is_generic','instance_name','ya-version']
    if keep_instance_names == True:
        attrs_to_remove = ['x', 'y', 'id', 'comment','name', 'inline', 'is_generic',  'ya-version']

    tags_to_remove =  ['yacodeblocks','comment','ya-version' ]

    weird_tags = ['teliko_apotelesma','507', 'element1', 'ITEM21','update_score2','customer_just_said','Clock1_FormatDate',]

    general_tags = ['FormatDate','setproperty','getproperty', 'ForgetLogin', 'GoHome', 'ShowMessageDialog','SendQuery',
                    'GotResult','StoreValue','EdgeReached','PositionChanged','AfterTextInput','Click', 'API_Key',
                    'GetMillis','TouchDown','Hour','Vibrate','Stop','Speak','DrawLine','Initialize','AfterGettingText',
                    'TurnCounterClockwiseIndefinitely', 'Month','CollidingWith','PointInDirection','Touched',
                    'MoveTo','DrawText','BackPressed','DayOfMonth','Start', 'Play','DrawLine','MoveBackwardIndefinitely'
                    , 'TurnClockwiseIndefinitely', 'Disconnect','Touched','GotValue', 'AfterPicking','Timer','Connect']


    for curr_element in blocks_xml_root.iter('*'):

        if curr_element.tag in weird_tags:
            print('found weird tag : ' + curr_element.tag + " in element: " + str(curr_element))

        # this element has attrib that need to be removed and transformed to additional elements
        if curr_element.tag in tags_to_remove:
            parent_map = {c: p for p in blocks_xml_root.iter() for c in p}
            parent_map[curr_element].remove(curr_element)
            continue


        if curr_element.attrib:
            for attr in curr_element.attrib.keys():

                if attr in attrs_to_remove:
                    continue

                if attr in weird_tags or curr_element.attrib[attr] in weird_tags:
                    print('found weird tag in attribs : ' + str(curr_element.attrib[attr]) + " in element: " + str(curr_element))

                for g_tag in general_tags:
                    if g_tag in attr:
                        attr = g_tag
                        continue
                    if g_tag in curr_element.attrib[attr]:
                        curr_element.attrib[attr] = g_tag
                        continue
                if 'set_get' in attr:
                    attr = 'component_set_get'
                if 'set_get' in curr_element.attrib[attr]:
                    curr_element.attrib[attr] = 'component_set_get'

                new_child = ETree.SubElement(curr_element,attr)
                another_child = ETree.SubElement(new_child, curr_element.attrib[attr])

            # reset the curr element attributes
            curr_element.attrib = {}

    return blocks_xml_root

def xml_to_parents_map(xml_root):
    parent_map = {}
    for index, p in enumerate(xml_root.iter()):
        p.attrib["key"] = p.tag + str(index)

        for c in p:
            parent_map[c] = p

    return parent_map

def xml_to_paths_map(xml_root, max_len):
    paths_map = {}
    parent_map = {}
    paths_map[xml_root] = (None, [])

    terminal_nodes_list = []

    for index, p in enumerate(xml_root.iter()):
        p.attrib["key"] = p.tag + str(index)

        path_to_p = paths_map[p][1] + [p]

        for c in p:
            paths_map[c] = (p, path_to_p)
            parent_map[c] = p

        # if p is a terminal node, also append path
        if (not p) and (p in parent_map):
            if p.attrib["key"] == "field87":
                x = 1
            terminal_nodes_list.append(p)
            path = paths_map[p][1]
            # path_copy = copy.deepcopy(path)
            if not isinstance(path, list):
                x = 1

            # insert only the path for the last node in the parents map
            parent_map[p] = (parent_map[p], path[:max_len + 1])


    return parent_map, terminal_nodes_list

def parent_map_to_list_maps(parent_map):
    parents_map_list = []

    for key, value in parent_map.items():

        # if key is a top block
        if value.tag == 'xml':
            top_block_parent_map = {}
            for element in key.iter():

                # no need to enter parent of top block (which is the 'xml' element)
                if element == key:
                    continue
                top_block_parent_map[element] = parent_map[element]

            parents_map_list.append(top_block_parent_map)
    return parents_map_list


def xml_to_parents_dict_list(xml_root):
    parent_maps_list = []
    element_to_map_index_dict = {}

    for index, p in enumerate(xml_root.iter()):
        if p.tag == 'Master':
            continue

        if p.tag == "xml":
            p.attrib["key"] = p.tag + str(index)

            for c in p:
                parent_map = {}
                parent_map[c] = p

                parent_maps_list.append(parent_map)

                element_to_map_index_dict[c] = len(parent_maps_list) - 1

        # not xml or master element
        else:
            p.attrib["key"] = p.tag + str(index)
            map_index = element_to_map_index_dict[p]
            parent_map = parent_maps_list[map_index]

            for c in p:
                parent_map[c] = p
                element_to_map_index_dict[c] = len(parent_maps_list) - 1

    return parent_maps_list


# given a parents map of elements in an xml tree of an app-inventor program,
# switch all the elements of type 'to_replace' to type 'replacement',
# returns True if a replacement took place, False otherwise
def replace_element(root, to_replace, replacement):

    for index, p in enumerate(root.iter()):
        if to_replace in p.tag:
            p.tag = replacement
            return True

    return False

def replace_all_elements(root, to_replace, replacement):
    result = False

    for index, p in enumerate(root.iter()):
        if to_replace in p.tag:
            p.tag = replacement
            result = True

    return result


def replace_elements_returncopy(root, to_replace, replacement):

    for index, p in enumerate(root.iter()):
        if to_replace in p.tag:
            origin_xml = copy.deepcopy(root)
            p.tag = replacement
            return True, origin_xml
    return False, None

def pathstring_to_list(path_string):
    path_as_list = []
    element = ""
    writing_word = False
    for index, char in enumerate(path_string):

        if writing_word:

            # just finished writing word
            if (char == ")"):
                writing_word = False
                path_as_list.append(element)
                element = ""
            # still need to write
            else:
                element += char

        # not in the middle of writing
        else:
            if char == "(":
                writing_word = True
            elif char == "^":
                path_as_list.append(char)
            elif char == "_":
                path_as_list.append(char)
    return path_as_list

def parentmap_to_nodeslist(parent_map):

    for parent in parent_map.values():
        # looking for the top block element
        if parent not in parent_map.keys():
            nodes_list = [parent]
            break

    for child,parent in parent_map.items():
        if child not in nodes_list:
            nodes_list.append(child)
        if parent not in nodes_list:
            nodes_list.append(parent)

    return nodes_list

def create_adj_matrix(nodes_list, parent_map):
    """ create an adjacency matrix of the tree, i.e. adj[i] is the list of
                child indices of node i.
    """
    adj_matrix = []

    for father_node in nodes_list:
        child_nodes = []

        for child in parent_map.keys():
            if parent_map[child] == father_node:
                child_nodes.append(nodes_list.index(child))

        adj_matrix.append(child_nodes)

    return adj_matrix

def nodes_to_tags_set(nodes_list):
    """ create a set of tags based on the tags of the nodes in the list
    """

    tags_set = set([])

    for node in nodes_list:
        tags_set.add(node.tag)

    return tags_set

def nodes_to_tags_list(nodes_list):
    """ create a list of tags based on the tags of the nodes in the list
    """

    tags_list = []

    for node in nodes_list:
        tags_list.append(node.tag)

    return tags_list