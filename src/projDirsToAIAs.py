# Python 3 code for converting a project-as-a-directory (of .scm and .bky files) 
# to a .aia file. It also converts directories of such projects to directories
# of .aia files. It was developed to help Kobi's team translate their data to .aia
# files so that they can use Evan Patton's .aiatools on the data. 
# 
# Author: Lyn Turbak
# 
# History: 
# [2020/04/16, lyn] 
#   * When translating directory of project-as-a-directory dirs, 
#     using renaming to guarantee two .aia files names aren't the same.
#     E.g. monitor.aia, monitor_2.aia, monitor_3.aia
#   * Shortend some function nams
# [2020/04/14, lyn] Created



import os, os.path
import zipfile
import json
import sys
import string

def projDirsToAIAs(projsDir, outputDir=None): 
    '''Convert a directory of project directories into new directory of .aias.
       If outputDir is specified, use that; otherwise create new one based on 
       name of projsDir'''
    if outputDir == None: 
        # Create outputDir automatically by adding -aias to projsDir
        outputDir = '{}-aias'.format(projsDir)
    appNamesSoFar = set()
    for projDir in os.listdir(projsDir):
        absProjDir = os.path.join(projsDir, projDir)
        projDirToAIA(absProjDir, outputDir, appNamesSoFar)

def projDirToAIA(projDir, outputDir, aiaFilenamesAlreadyUsed=set()):
    '''Convert the given project directory into a .aia file that is written to 
       the output directory outputDir. Avoid app names in aiaFilenamesAlreadyUsed
       by renaming. Return the .aia file name'''
    projBase = os.path.basename(projDir)
    appName = getAppName(projDir, aiaFilenamesAlreadyUsed)
    aiaFilename = '{}.aia'.format(appName)
    aiaPath = os.path.join(outputDir, aiaFilename)
    makeDirIfDoesntAlreadyExist(outputDir)
    try:
        print('Converting {} to {} ... '.format(projBase, aiaFilename), end='')
        zipForAIA = zipfile.ZipFile(aiaPath,  'w', zipfile.ZIP_DEFLATED)
        aiaZipPath = 'src/appinventor/ai_unknown/{}'.format(appName)
        # Add all .scm and .bky files to .aia (zip) file
        for projFile in os.listdir(projDir):
            absFile = os.path.join(projDir, projFile) # Absolute path 
            zipForAIA.write(absFile, os.path.join(aiaZipPath, projFile))
        # Add project.properties file to .aia (zip) file
        propertyContent = propertiesTemplate.format(
            appName1=appName, 
            appName2=appName, 
            appName3=appName
            )
        zipForAIA.writestr('youngandroidproject/project.properties', propertyContent)
        zipForAIA.close()
        print('done')
        return(aiaFilename)
    except: 
        print("***Error encountered when creating {}; no AIA file written.\n{}".
              format(aiaFilename, errMsg()))
        # Returns None in this branch

def getAppName(projDir, appNamesAlreadyUsed): 
    try: 
        screen1Dict = scmJSONContents(projDir, 'Screen1.scm')
        appName = screen1Dict['Properties']['AppName']
        return nameNotIn(appNamesAlreadyUsed, appName)
    except: 
        base = os.path.basename(projDir)
        filenameWithoutExtension = os.path.splitext(base)[0]
        generatedAppName = validAppName(filenameWithoutExtension)
        print("***Can't find user app name for {}; using {} instead".
              format(filenameWithoutExtension, generatedAppName))
        return nameNotIn(appNamesAlreadyUsed, generatedAppName)

def nameNotIn(nameSet, name):
    '''Return a version of name not in nameSet, possibly adding number at end to avoid conflict.
       Modify nameSet to contain the final name.'''
    if name not in nameSet:
        nameSet.add(name)
        return name
    else:
        n = 2 # version number 
        newName = '{}_{}'.format(name, n)
        while newName in nameSet:
            n += 1
            newName = '{}_{}'.format(name, n)
        print('Renaming app name {} to {} to avoid a name conflict with existing app'.
              format(name, newName))
        nameSet.add(newName)
        return newName

def validAppName(name):
    '''Valid app names must start with letter and can contain only letters,
       numbers, and underscores.'''
    return 'proj_{}'.format(transformName(name))

def transformName(name):
    return ''.join([c2 for c2 in [transformChar(c1) for c1 in name] 
                    if isValidAppNameChar(c2)])
            

def isValidAppNameChar(ch):
    return ch == '_' or ch in string.ascii_letters or ch in string.digits

def transformChar(ch):
    '''Turn hyphens into underscores; keep all other chars the same'''
    return '_' if ch == '-' else ch

# Modified from ai2summarizer2.py
def scmJSONContents(projDir, scmFileName):
    absScmFileName = os.path.join(projDir, scmFileName) # Absolute file namex
    with open(absScmFileName, 'r') as scmFile:
        scmLines = scmFile.readlines()
        if (len(scmLines) == 4
            and scmLines[0].strip() == '#|'
            and scmLines[1].strip() == '$JSON'
            and scmLines[3].strip() == '|#'):
            try:
                contents= json.loads(scmLines[2])
            except:
                contents = {'Properties':{}}
                print("***Malformed scm file {} in project dir {}. Error: {}"
                      .format(scmFileName, projDir, errMsg()))
        else:
            try:
                contents = json.loads(scmLines)
            except:
                contents = {'Properties':{}}
                print("***Malformed scm file {} in project dir {}. Error: {}"
                      .format(scmFileName, projDir, errMsg()))
        return contents

def errMsg():
    '''Return an error message string from sys.exc_info()'''
    errType, errVal = sys.exc_info()[:2]
    return '{}: {}'.format(errType, errVal)

def makeDirIfDoesntAlreadyExist(dirName):
  '''Make a directory named dirName if it doesn't exist. Do nothing if it does exist.'''
  if not os.path.exists(dirName):
      os.mkdir(dirName)

# The following string is a template for the youngandroidproject/project.properties file.
# Each of the "holes" appName1, appName2, and appName3 should be filled in with the 
# same application name. Note that we use ai_unknown for unknown account. 
propertiesTemplate = \
"""main=appinventor.ai_unknown.{appName1}.Screen1
name={appName2}
assets=../assets
source=../src
build=../build
versioncode=1
versionname=1.0
useslocation=False
aname={appName3}
"""

if __name__=="__main__":
    if len(sys.argv) == 2: 
        projDirsToAIAs(sys.argv[1])
    elif len(sys.argv) == 3: 
        projDirsToAIAs(sys.argv[1], sys.argv[2])
    else: 
        print(
'''Usage: python3 convertProjectDirectoriesToAIAs.py <projsDir> {<outputDir>}'
  where <projsDir> is a directory containing projects-as-directories'
  and (optionally) <outputDir> is a directory for resulting .aia files'
  (it will be created if it does not already exist).
  If <outputDir> is not specified, it will be created by adding -aias to <projsDir>''')
# Examples that works in Lyn's directory structure:
# 
# python3 projDirsToAIAs.py jis-projects-for-kobi/extracted1_102690152548738168665 aiaFiles1
# 
# python3 projDirsToAIAs.py jis-projects-for-kobi/extracted1_103327667211985548619
