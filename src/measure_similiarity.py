
import pandas as pd
import numpy as np
import CONFIG
from scipy.spatial import distance

from math import sqrt
from sklearn.cluster import KMeans
from sklearn.cluster import SpectralClustering

from sklearn.metrics.cluster import completeness_score
from sklearn.metrics.cluster import homogeneity_completeness_v_measure
import matplotlib.pyplot as plt
from sklearn.cluster import DBSCAN
from sklearn.cluster import OPTICS





def square_rooted(x):
    return round(sqrt(sum([a * a for a in x])), 3)


def cosine_similarity(x, y):
    numerator = sum(a * b for a, b in zip(x, y))

    denominator = square_rooted(x) * square_rooted(y)
    return round(numerator / float(denominator), 3)

def main():
    embedings_path = r"./101_untouched_embeddings.csv"
    df = pd.read_csv(embedings_path)

    true_labels = []
    for index, row in df.iterrows():
        if 'pong' in row['category']:
            true_labels.append(0)

        if 'misc' in row['category']:
            true_labels.append(1)

        if 'calc' in row['category']:
            true_labels.append(2)

        if 'time' in row['category']:
            true_labels.append(3)

    df = df.drop(["category", "project_name"], axis=1)



    # clustering = MeanShift().fit(df)
    # print(clustering.labels_)

    kmeans = KMeans(n_clusters=6,).fit(df[:75])
    print(kmeans.labels_)

    print(homogeneity_completeness_v_measure(true_labels[:75], kmeans.labels_))

    model = SpectralClustering(n_clusters=6, affinity='nearest_neighbors',
                               assign_labels='kmeans').fit(df[:75])

    print(model.labels_)
    print(homogeneity_completeness_v_measure(true_labels[:75], model.labels_))

    dbscan = DBSCAN(eps=3, min_samples=7).fit(df[:75])
    print(dbscan.labels_)
    print(homogeneity_completeness_v_measure(true_labels[:75], dbscan.labels_))

    optics = OPTICS(min_samples= 7).fit(df[:75])
    print(optics.labels_)
    print(homogeneity_completeness_v_measure(true_labels[:75], optics.labels_))



    #
    # clustering = AffinityPropagation().fit(df)
    # print(clustering.labels_)

    # df = pd.read_csv(embedings_path)
    # generate_euclid_sim_csv(df)

    # sum_distance_per_category()


def sum_distance_per_category():
    df = pd.read_csv("similarity_euclid_matrix.csv")
    master_row = df.iloc[100]
    pong_sum = 0
    misc_sum = 0
    calc_sum = 0
    time_sum = 0
    for index, value in master_row.items():

        if index == "category" or index == "project_name":
            continue

        curr_row = df.iloc[int(index)]
        if 'pong' in curr_row['category']:
            pong_sum += value

        if 'misc' in curr_row['category']:
            misc_sum += value

        if 'calc' in curr_row['category']:
            calc_sum += value

        if 'time' in curr_row['category']:
            time_sum += value
    x = 1


def generate_cosine_sim_csv(df):
    categs = df["category"]
    project_names = df["project_name"]
    df = df.drop(["category", "project_name"], axis=1)
    similarity_df = []
    for index1, row1 in df.iterrows():
        column = []

        for index2, row2 in df.iterrows():
            dist = cosine_similarity(row1.tolist(), row2.tolist())
            column.append(dist)

        similarity_df.append(column)
    similarity_df = pd.DataFrame(similarity_df)
    similarity_df["category"] = categs
    similarity_df["project_name"] = project_names
    similarity_df.to_csv("similarity_cosine_matrix.csv", index=False)

def generate_euclid_sim_csv(df):
    categs = df["category"]
    project_names = df["project_name"]
    df = df.drop(["category", "project_name"], axis=1)
    similarity_df = []
    for index1, row1 in df.iterrows():
        column = []

        for index2, row2 in df.iterrows():
            dist = distance.euclidean(row1.tolist(), row2.tolist())
            column.append(dist)

        similarity_df.append(column)
    similarity_df = pd.DataFrame(similarity_df)
    similarity_df["category"] = categs
    similarity_df["project_name"] = project_names
    similarity_df.to_csv("similarity_euclid_matrix.csv", index=False)


if __name__ == "__main__":
    main()
